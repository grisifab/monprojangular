import { User } from '../models/User.model';
import { Userbis } from '../models/Userbis.model';
import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
const optionRequete = {
  headers: new HttpHeaders({ 
    'Access-Control-Allow-Origin':'*'
  })
};

@Injectable()
export class UserService {

  private usersUrl: string;
  
  private users: User[] = [
    new User('Will', 'Alexander', 'will@will.com', 'jus d\'orange', ['coder', 'boire du café'])
  ];

  userSubject = new Subject<User[]>();

  private userbis = new Userbis('Bilou','bg@mail.com');

  constructor(private httpClient: HttpClient) { 
    this.usersUrl = 'http://localhost:8080/users';
  }

  emitUsers() {
    this.userSubject.next(this.users.slice());
  }

  addUser(user: User) {
    this.users.push(user);
    this.emitUsers();
  }

}